package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void shouldAllowPositiveIntegers() {
        //Arrange
        String firstName = "Nuno";
        String lastName = "Ribeiro";
        String description = "Student";
        int jobYears = 10;
        String email = "email@email.com";
        Employee newEmployee = new Employee(firstName, lastName, description, jobYears, email);
        //Act
        boolean result = newEmployee.validateJobYears(jobYears);
        //Assert
        assertTrue(result);
    }

    @Test
    void shouldNotAllowNegateInteger() {
        //Arrange
        String expectedMessage = "Invalid Job Years";
        String firstName = "Nuno";
        String lastName = "Ribeiro";
        String description = "Student";
        int jobYears = -10;
        String email = "email@email.com";
        Employee newEmployee = new Employee(firstName, lastName, description, jobYears, email);
        //Act
        boolean result = newEmployee.validateJobYears(jobYears);
        //Assert
        assertFalse(result);
    }

    @Test
    void shouldValidateEmail(){
        //Arrange
        String expectedMessage = "Invalid Job Years";
        String firstName = "Nuno";
        String lastName = "Ribeiro";
        String description = "Student";
        int jobYears = 10;
        String email = "email@email.com";
        Employee newEmployee = new Employee(firstName, lastName, description, jobYears, email);
        //Act
        boolean result = newEmployee.validateEmail(email);
        //Assert
        assertTrue(result);
    }

    @Test
    void shouldNotValidateEmail(){
        //Arrange
        String expectedMessage = "Invalid Job Years";
        String firstName = "Nuno";
        String lastName = "Ribeiro";
        String description = "Student";
        int jobYears = 10;
        String email = "email";
        Employee newEmployee = new Employee(firstName, lastName, description, jobYears, email);
        //Act
        boolean result = newEmployee.validateEmail(email);
        //Assert
        assertFalse(result);
    }

    @Test
    void shouldNotValidateEmailWithNoNameInBeggining(){
        String firstName = "Nuno";
        String lastName = "Ribeiro";
        String description = "Student";
        int jobYears = 10;
        String email = "@email.com";
        Employee newEmployee = new Employee(firstName, lastName, description, jobYears, email);
        //Act
        boolean result = newEmployee.validateEmail(email);
        //Assert
        assertFalse(result);
    }
}