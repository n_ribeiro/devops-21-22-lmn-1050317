CA1 - Technical Report
======================
###CA1 - Part 1

_**1. Copy the code of the Tutorial React.js and Spring Data REST Application into a
new folder named CA1**_

>**Create a new directory using the command:**
> 
> mkdir ./CA1

>**clone the repository into the new folder:**
> 
>cd ./CA1 and git clone https://github.com/spring-guides/tut-react-and-spring-data-rest

_**2. Commit the changes (and push them)**_

>git commit -m "Initial Version"
> 
>git push origin master

_**3. We should use tags to mark the versions of the application. You should use a
pattern like: major.minor.revision (e.g., 1.1.0).
Tag the initial version as v1.1.0. Push it to the server.**_

>git tag -a v1.1.0 -m "Initial Version"
> 
>git push origin master v1.1.0

_**4. Lets develop a new feature to add a new field to the application. In this case, lets
add a new field to record the years of the employee in the company (e.g., jobYears).**_

>You should add support for the new field.
You should also add unit tests for testing the creation of Employees and the validation
of their attributes (for instance, no null/empty values). For the new field, only integer
values should be allowed.
You should debug the server and client parts of the solution.
When the new feature is completed (and tested) the code should be committed and
pushed and a new tag should be created (e.g, v1.2.0).

* Having done the changes and adding tests I committed the changes in this push

>git commit -m "Resolves Issue #3 and #4"
> 
>git push origin master v1.1.3

* When debugging the client side of the solution a mistake was found and corrected

>git commit -m "Addressing Issue #5"
 
* After resolving the problem:

> git commit -m "Resolve Issue #5"
> git push origin master 
 
* Finishing the assignment:

> git tag -a ca1-part1
> git commit -m "Resolve Issue #6"
> git push origin master
> git push origin master ca1-part1

_**5. Difficulties found when working with git**_

_*5.1. Reverting to a last commit*_

>**git reset --soft HEAD~1**
>
> This command deletes the last from the Git history

By typing this command on the command line i reverted back to the state before a mistake was made,
but by doing this i deleted the readme.md file.

So I did the next commands to restore the deleted file.

_*5.2. Deleting a file and commit and push the changes*_

Having deleted the readme.md file and only later noticing the mistake with these commands I was able to restore the file:
> **git log -- readme.md**
>
> with this command I was able to find the commit where the file was deleted.
> The output of the command was the commit hashcode
>
> **git checkout <commit hash> -- readme.md**
>
> With checkout i was able to restore the file back onto the folder.


###CA1 - Part 2

_**You should develop new features in branches named after the feature. Create a
branch named "email-field" to add a new email field to the application.**_

* Creating the branch email-field

> **git branch email-field**
> 
> This command creates a new branch.
> 
> **git checkout email-field**
> 
> And this next command we switch to the newly created branch

* After developing the new feature for the app we are now ready to commit the changes to the repository:

> **git commit -a -m "Created the email field. Addresses #7"**

* Creating Unit Testing:

> **git commit -a -m "Added unit tests. Resolves Issue #7 and addresses Issue #8"**
> **git tag -a v1.3.0 -m "Create email field"**
> 
> Created a new tag for the revision made

* Return to the Master Branch

>**git checkout master**

* Merge the Master branch and the email-field created for the new feature

>**git merge email-field**

* Push the changes to the repository

> **git push origin master v1.3.0**

_**You should also create branches for fixing bugs (e.g., "fix-invalid-email").**_

> **git branch fix-invalid-email**
> 
> Has with the point before we first create the branch we want to do our work on,
> 
> **git checkout fix-invalid-email**
> 
> move to the newly created branches
> 
> **git commit -a -m "Added fix for invalid email. Addresses Issue #10"**
> 
> make the changes necessary to the code,
> 
> **git commit -a -m "Added Unit testing for invalid email fix. Addresses Issue #8 Resolves Issue #10"**
> 
> Added the unit tests to make sure the code is working has it should
> 
> **git checkout master**
> 
> To return again to the Master branch,
> 
> **git merge fix-invalid-email**
> 
> Create the tag for the revision,
> 
> **git tag v1.3.1**
> 
> and finally push the changes to the repository
> 
> **git push origin master v1.3.1**


_**Alternative technological solution for version controll**_

For the alternative I chose "Fossil" software created D. Richard Hipp.
Like "Git" it is a cross-platform Distributed Version Control System.
But by tipping **"fossil ui"** a user is able to launch a web browser to display the information and history of the project.

A few differences between fossil and git:

| GIT | FOSSIL |
| --- |--------|
| HEAD | TIP    |
| MASTER | TRUNK  |
| ISSUE | TICKET |

It requires the download of a "stand-alone" executable, and after it is finished a user just has to put the file in the $PATH

At first I was not able to correctly push the changes to the webserver that hosts the code, but after a few tries this is how i was able to manage it:

> Register at "https://chiselapp.com" and create a new repository
> 
> clone the repository to a local folder
> 
> **fossil clone https://chiselapp.com/user/nuno-ribeiro/repository/ca1-alternative ca1-alternative.fossil**
> 
> Turn the autosync setting off (autosync makes an automatic push to the repository)
> 
> **fossil settings --global autosync 0**
> 
> Copy the folders of the React tutorial to the local folder where the .fossil file is located
> And make the first commit of the changes
> 
> **fossil commit -m "Initial commit" --no-warnings**
> 
> (The flag --no-warnings omit all warnings about file contents )
> 
> and finally pushed the changes to the repository
> 
> **fossil push https://<USER>@chiselapp.com/user/<USER>/repository/<CREATED REPOSITORY>**
> 
> it will ask for the password of the created repository

**Creating Issues and addressing in the commit message**

>After creating a ticket in the Chiselapp website and resolving the issue within the code base:
> 
> **fossil commit -m "Added job years field to the app [TICKET HASH]"**
> 
> **fossil ticket set [TICKET HASH] status Fixed**

**Creating a branch**

> **fossil branch new [BRANCH NAME] [BRANCH NAME FROM WHERE IT IS CREATED]**
> 
> **fossil update [BRANCH-NAME]**
> 
> Do the necessary changes to the code and merge the branches
> 
> **fossil merge [BRANCH-NAME]**
> 
> Commit and push the changes
> 
> **fossil commit -m "Added email field to the app [TICKET HASH]"**
> 
> **fossil ticket set [TICKET HASH] status Fixed**
> 
> **fossil push fossil push https://<USER>@chiselapp.com/user/<USER>/repository/<CREATED REPOSITORY>**

It is noteworthy to say that usually with **Fossil** it is normal to first do the changes to the code and then
commit to a new branch

Work done can be checked and corrected here:

https://chiselapp.com/user/nuno-ribeiro/repository/ca1-alternative/index