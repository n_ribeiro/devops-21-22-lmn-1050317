CA3 - Technical Report
======================
# CA3- Part 1 - Setting up and running a Virtual Machine on Oracle VM VirutalBox

## 1 - Download and install Oracle VM VirtualBox

Go to [Oracle VM VirtualBox](https://www.virtualbox.org/) and download the latest version that fits your machine(Windows hosts,  OS X hosts, Linux Distributions, ...)

Once the download is finished follow the instructions.

## 2 - Setting up a virtual Box

For this this assignment we are
1. installing a mininal version of [Ubuntu](https://help.ubuntu.com/community/Installation/MinimalCD);
2. Setting the VM with a 2048 MB of RAM;
3. Setting Network Adapter 1 as Nat
4. Setting Network Adapter 2 as Host-only Adapter (vboxnet0)

### 2.1 - Installing the Mininal version of Ubuntu

Follow the link above and download the ISO image.
Create a new VM and name it as you like.
Hint: If you name it Ubuntu, Virtual-Box will know that you will be installing a version of Ubuntu

![VMBOX-1](/Images/VMBOX-1.png)

Press Next to go the following step

### 2.2 - Setting the VM with a 2048 MB of RAM

The new VM will be set to the recommend size of 1024 MB of RAM memory

![VMBOX-2](/Images/VMBOX-2.png)

Set it to 2048 MB and go the next step


### 2.4 - Setting the VM with a 2048 MB of RAM

Go to File -> Host Network Manager or (Ctrl+H)

![VMBOX-3](/Images/VMBOX-3.png)

Create a new Host-only network and it will be added to the list

For the purpose of this assignment we will assign the IPV4 address to: 192.168.56.1

## 3 - Start the VM

Before starting the VM we need to had the ISO image we download to the optical drive

![VMBOX-4](/Images/VMBOX-4.png)

Now start the VM

![VMBOX-5](/Images/VMBOX-5.png)

Once the installation has finished remove the ISO image from the Optical Drive and restart the VM

## 4 - Setting up the VM

First we need to update the package repositories

```
sudo apt-get update
```

Install thew Network Tools

```
sudo apt install net-tools
```

Edit the network configuration file to setup the IP

```
sudo nano /etc/netplan/01-netcfg.yaml
```

And add theese changes (make sure to add the same IP as the second network adapter)

```
network:
    version: 2
    renderer: networkd
    ethernets:
        enp0s3:
            dhcp4: yes
        enp0s8:
            addresses:
                - 192.168.56.5/24
```

Hint: when identing use spaces instead of tabs

Apply the changes

```
sudo netplan apply
```

Install openssh-server 

```
sudo apt install openssh-server
```

Enable Password authentication for ssh

```
sudo nano /etc/ssh/sshd_config
```
uncomment the line PasswordAuthentication yes
```
sudo service ssh restart
```

Install FTP server

```
sudo apt install vsftpd
```

Enable write access for vsftpd

```
sudo nano /etc/vsftpd.conf
```
uncomment the line write_enable=YES
```
sudo service vsftpd restart
```

We can now access the VM thru ssh
```
ssh [username]@192.168.56.5
```

Install GIT
```
sudo apt install git
```

Install Java
```
sudo apt install openjdk-8-jdk-headless
```

## 5 - Install and Execute the Spring Tutorial

Clone the repository
```
git clone https://github.com/spring-guides/tut-react-and-spring-data-rest.git
```

Change to the Basic folder of the tutorial

```
cd tut-react-and-spring-data-rest/basic
```

Build and Execute the applicatiob

```
./mvnw spring-boot:run
```

We should now have the application frontend running at
```
192.168.56.5:8080
```
If look into the web browser
![VMBOX-6](/Images/VMBOX-6.png)
**SUCCESS**

## 6 - Running the applications on the Repository

Clone the repository
```
git clone https://N_Ribeiro@bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317.git
```

Install Maven
```
sudo apt install maven
```

Install Gradle
```
sudo apt install gradle
```

### 6.1 - Running the Spring Boot tutorial

cd to the basic folder of the repository
```
cd devops-21-22-lmn-1050317/CA1/basic
```

Run the application with maven
```
mvn spring-boot:run
```

Now on the web browser
```
192.168.56.5:8080
```
![VMBOX-7](/Images/VMBOX-7.png)

### 6.2 - Running the gradle basic demo

cd into the gradle_basic_demo folder

```
cd gradle_basic_demo
```

Run the command

```
./gradlew runServer
```
![VMBOX-8](/Images/VMBOX-8.png)

Once the server is running, on you local machine change the the runClient Task so that it runs on the ip of the VM

```
task runClientVM(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on localhost:59055 "

    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatClientApp'

    args '192.168.56.5', '59055'

}
```

And run the client

```
gradle runClientVM 
```

The App is now running

![VMBOX-9](/Images/VMBOX-9.png)

## 7 - Finishing the assignment

Once the assignment is finished tag the repository with the tag ca3-part1