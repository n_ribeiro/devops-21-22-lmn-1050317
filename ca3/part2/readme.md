CA3 - Technical Report
======================

# CA3- Part 2 - Virtualization with Vagrant

## 1 - Downloading and installing Vagrant

Go to [Vagrant Website](https://www.vagrantup.com/downloads.html) and download the latest version suitable for your machine and intall it.

To check if Vagrant was installed run:

```
vagrant --version
```

![VAGRANT1](/images/VAGRANT1png.png)

## 2 - CA2-Part2 [107324e](https://bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317/commits/107324e23d227d13b3411fd995b784a6581d298c)

Copy the src folder from the CA2-Part2 to the new project folder

Add new configuration file by running:

```
vagrant init envimation/ubuntu-xenial
```

By studying the provided [Vagrantfile](https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/Vagrantfile) provided we can see  that we are going to use to VM to run the application:

- web: used to run tomcat and the spring boot basic application

- db: used to execute the H2 server database

Update the generated Vagrantfile and commit to the repository

## 3 - Updating the build.gradle and Vagrantfile [fcf38e9](https://bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317/commits/fcf38e92a6f18a1e4b2e38785f2170a0ede1b8d7)

### 3.1 - Vagrantfile

There are few lines that need to be changed before commiting the changes

1. Since the VM only runs Java version 8 we can change the line that downloads Java to the version

```
sudo apt-get install openjdk-8-jdk-headless -y
```

This line is not obligatory since the package repositories will download the correct Java for the VM

2. Change the line where the VM will clone your repository

```
git clone https://N_Ribeiro@bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317.git
```

And change the cd line just below

```
cd devops-21-22-lmn-1050317/ca3/part2
```

3. And for last, change the path for the war file that will be generated, in this case:

```
sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
```

### 3.2 - build.gradle

As we did for Vagrantfile, there are some lines we need to update so that the build runs.

1. Changed Java Version from 11 to 8

```
id "org.siouan.frontend-jdk8" version "6.0.0"
```

2. Adds Support to war file

```
id "war"
```

3. Change sourceCompatibility from 11 to 8

```
sourceCompatibility = '8'
```

4. Add support to war file to deploy tomcat

```
providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
```

5. Configuring node.js

Update the Frontend section

```
frontend {
	nodeVersion = "12.13.1"
	assembleScript = 'run webpack'
	//assembleScript = "run build"
	//cleanScript = "run clean"
	//checkScript = "run check"
}
```

## 3 - Updating the package.json file [68a336f](https://bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317/commits/68a336fe495e608a4c046d19f2ad3b12b3256e8c)

Update the Scripts section

```
"scripts": {
    "watch": "webpack --watch -d",
    "webpack": "webpack"
  },
```

## 4 - Creating and adding the ServletInitializer.java class

Create a new Java file at **src/main/java/com/greglturnquist/payroll** and name it 

```
ServletInitializer.java
```

With the following code

```
package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }

}
```
## 5 - Update the application.properties file [d970cfc](https://bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317/commits/d970cfc449ad91166ac681bb5970d00f2510f4d8)

Add the lines

```
server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```
## 6 - Update the app.js file [865fb28](https://bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317/commits/865fb28d2b3c835918780cb118ec4be4c0fd0a39)

Update the path on the GET method, in this case:

```
componentDidMount() { // <2>
		client({method: 'GET', path: '/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
			this.setState({employees: response.entity._embedded.employees});
		});
	}
```

## 7 - Start Vagrant 

If everything is done correctly we can start the vagrant file

```
vagrant up
```

### 7.1 - Changes done to the Vagrantfile

When first deploying Vagrant a error ocurred when it wanted to copy the war file

This happened because on the example given the war file was named:

```
basic-0.0.1-SNAPSHOT.war
```

By connecting thru ssh to the web VM 

```
vagrant ssh web
```

the name of the war file found was:

```
react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war
```

After commiting the changes to the repository we need to reload vagrant with the changes done

```
vagrant reload --provision
```

If we go to the Oracle VM application, we can see that the two VM are running

![VAGRANT5](/images/VAGRANT5.png)

## 8 -  Accessing the application

If everything went according to plan

### 8.1 - Application frontend

we can access by going to:

```
http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/
```

or

```
http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/
```

And we can see the application frontend

![VAGRANT2.png](/images/VAGRANT2.png)

### 8.2 - Application Backend

we can access by going to:

```
http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console
```

or

```
http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console
```

And can see the login page for the h2 database

![VAGRANT3.png](/images/VAGRANT3.png)

For the connection string use:

```
jdbc:h2:tcp://192.168.56.11:9092/./jpadb
```

And finally we can access the database

![VAGRANT4.png](/images/VAGRANT4.png)

## 9 - Pausing and killing the VM's generated

We can pause the VM's by running the command

```
vagrant halt
```

And can delete the VM's by running

```
vagrant destroy -f
```

***

# CA3- Part 2 - Virtualization with Vagrant - Hypervisor Variant

## 1 - Choosing, downloading and installing the hypervisor alternative

For the hypervisor variant VMware Workstation was chosen.

Like the Oracle VM Virtualbox, VMware workstation is a hypervisor type 2. It's biggest advantages are:

- Easy to use
- Simple interface
- Compatible with many systems
- Easy to install


Disadvantages:

- Not free
- Expensive
- Is not open source making it sometimes difficult to fix major bugs
- Slower to load VM

## 2 - Setting up the environment for the new build

So we can do a clean build, I create a new folder inside the CA3 folder named:

```
./part2-alt
```

once there, i copied the files from the part2 into this folder

## 3 - Setting up VMware with Vagrant

Install the plugin for the VMware

```
vagrant plugin install vagrant-vmware-desktop
```

Update the plugin

```
vagrant plugin update agrant-vmware-desktop
```

We also need to download install the plugin from

[Download Vagrant vmware Utility](https://www.vagrantup.com/vmware/downloads)

## 4 - Update the Vagrantfile[36b9bc9](https://bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317/commits/36b9bc966c5a540abc19b58dd73c5ee069a35863#chg-ca3/part2-alt/Vagrantfile)

Since the envimation/ubuntu-xenial box is supported by VMware there is no need to choose a new box.

For vagrant to run VMware we need to change the provider in the Vagrantfile

```
Vagrant.configure("2") do |config|
  config.vm.box = "envimation/ubuntu-xenial"
  config.vm.provider "vmware_workstation" do |v|

    v.gui = true

  end  

  # This provision is common for both VMs
  config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update -y
    sudo apt-get install iputils-ping -y
    sudo apt-get install -y avahi-daemon libnss-mdns
    sudo apt-get install -y unzip
    sudo apt-get install openjdk-8-jdk-headless -y
    # ifconfig
  SHELL
.....
```

## 5 - Run Vagrant

If everything is correct we can start vagrant.

```
vagrant up
```

Since we copied the files from the part2 an error occurred:

![VAGRANT6.png](/images/VAGRANT6.png)

So, we need to change the IP on theese files:
[7098393](https://bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317/commits/70983934008c13eb154f4e59a9c88bcbdbb47e3f)

```
Vagrantfile
application.properties
```

Running again Vagrant with the changes made:

```
vagrant reload --provision
```

Using the new ips to check the frontend and Database:

```
http://192.168.33.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console/

http://192.168.33.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/
```

We can see all is well!

![VAGRANT7.png](/images/VAGRANT7.png)

(A change was made on the DataBaseLoader.java class file to see if the VM's where running correctly on Git commit   - [091ca53](https://bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317/commits/091ca538a5c5bcfa2088a05ac8e6a228afba30f8))