CA4 - Technical Report
======================

## CA4- Part 1 - Containers with Docker

### 1. Analysis, design and implementation

For Part 1 of this assignment, the goal is to create **Docker** images and running **Containers** using the chat application from [CA2](https://bitbucket.org/luisnogueira/gradle_basic_demo/)

So the assignment is divided in a few goals:

- The goal is to be able to package and execute the chat server in a container
- You should create a docker image (i.e., by using a Dockerfile) to execute the chat server

    - In this version you should build the chat server "inside" the Dockerfile
    - In this version you should build the chat server in your host computer and copy the jar file "into" the Dockerfile1

- You should tag the image and publish it in docker hub
- You should be able to execute the chat client in your host computer and connect to the chat server that is running in the container
- At the end of this assignment mark your repository with the tag ca4-part1.

### 1.1. A brief explanation of containers

Containers are a type of System Virtualization that allows to isolate memory, filesystem and network resources.

In comparison to Virtual Machines (that require their own Operating system), Containers leverage and share a single operating system and are more lightweight (less space, easy to create and use) [^1]

### 1.2 Docker

For the class assignment we will be using **Docker**.

**Docker** is a set of platfrom as a service (PaaS) [^2] products that use OS-level virtualization to deliver software in pacckages called containers.

- **Docker images and containers** [^1]

    - **Image**: An image is a template used to create a docker container

    - **Container**: Is a runnable instance of an image

### 2. Analysis, design and implementation

#### 2.1. Create a Docker image

The first step needed is to head over to [Docker](https://www.docker.com/get-started/), download and install **Docker Desktop**.

To check to see if Docker was correctly installed:

```
docker --version

or

docker info
```

![image1](../report_images/docker1.png)

Once we can see that Docker has been correctly instaled we can start to create a **Dockerfile**.

**Dockerfiles** are used to configure Docker images, thru a set of commands a user would call on the command line.

The following instructions are used as commands for the dockerfile: [^3]

```
- ADD: The ADD instruction copies new files, directories or remote file URLs from "src" and adds them to the filesystem of the image at the path "dest".
- ARG: The ARG instruction defines a variable that users can pass at build-time to the builder with the docker build command using the --build-arg "varname"="value" flag.
- CMD: The main purpose of a CMD is to provide defaults for an executing container. (there acan be only one CMD command in a dockerfile)
- COPY: The COPY instruction copies new files or directories from "src" and adds them to the filesystem of the container at the path "dest"
- ENTRYPOINT: An ENTRYPOINT allows you to configure a container that will run as an executable.
- ENV: The ENV instruction sets the environment variable "key" to the value "value".
- EXPOSE: The EXPOSE instruction informs Docker that the container listens on the specified network ports at runtime.
- FROM: The FROM instruction initializes a new build stage and sets the Base Image for subsequent instruction
- HEALTHCHECK: The HEALTHCHECK instruction tells Docker how to test a container to check that it is still working.
- LABEL: The LABEL instruction adds metadata to an image. 
- RUN: The RUN instruction will execute any commands in a new layer on top of the current image and commit the results.
- SHELL: The SHELL instruction allows the default shell used for the shell form of commands to be overridden.
- STOPSIGNAL: The STOPSIGNAL instruction sets the system call signal that will be sent to the container to exit.
- USER: The USER instruction sets the user name (or UID) and optionally the user group (or GID) to use when running the image and for any RUN, CMD and ENTRYPOINT instructions that follow it in the Dockerfile.
- VOLUME: The VOLUME instruction creates a mount point with the specified name and marks it as holding externally mounted volumes from native host or other containers.
- WORKDIR: The WORKDIR instruction sets the working directory for any RUN, CMD, ENTRYPOINT, COPY and ADD instructions that follow it in the Dockerfile.
- ONBUILD: The ONBUILD instruction adds to the image a trigger instruction to be executed at a later time, when the image is used as the base for another build. 
```
___

So for this assignment there was a need to create two _dockerfiles_ [d2f1644](https://bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317/commits/d2f16444ee63fce05d24ef6018fbf660f125aac9)

### Solution 1

On the first one we are building the chat server "inside" the dockerfile

```
FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install openjdk-11-jdk-headless -y
RUN apt-get install git -y
RUN git clone https://N_Ribeiro@bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317.git

WORKDIR /devops-21-22-lmn-1050317/ca2/Part1/gradle_basic_demo

RUN chmod u+x gradlew
RUN ./gradlew build
CMD ./gradlew runServer

EXPOSE 59055
```

Next we run the command:
```
docker build -t solution1:v01 .
```

(we are using the -t flag so we can name the image solution1 and add the tag v01)

![docker2](../report_images/docker2.png)

we can see the created image running the command:

```
docker images

or 

docker image list
```

![docker3](../report_images/docker3.png)

Once the image is created we can build Container that will execute the image and run the chat server

```
docker run -p 59055:59055 solution1:v0.1
```

(The flag -p tells the container to expose the port 59005 to the outside)

![docker4](../report_images/docker4.png)

Going to the CA2/Part1 folder and running:

```
./gradlew runClient
```

![docker5](../report_images/docker5.png)

![docker6](../report_images/docker6.png)

Now, to stop the container we will need to find the container ID and stop it:

```
docker container list --all
```

![docker7](../report_images/docker7.png)

and now the command:

```
docker kill b3343419a982
```

if we run again the command to list all the container we'll see that's not running anymore

![docker8](../report_images/docker8.png)

### Solution 2

```
In this version you should build the chat server in your host computer and copy the jar file "into" the Dockerfile
```

Starting with the **dockerfile**:

```
FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install openjdk-11-jdk-headless -y
RUN mkdir /dockerbuild

WORKDIR /dockerbuild

COPY ./basic_demo-0.1.0.jar /dockerbuild/

CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59055

EXPOSE 59055
```
Now the process is the same fo the solution 1

Building the **Image**

```
docker build -t solution2:v0.1 .
```

![docker9](../report_images/docker9.png)

```
doker images
```

![docker10](../report_images/docker10.png)

Running the image to create the container

```
docker run -p 59055:59055 solution2:v0.1
```

![docker11](../report_images/docker11.png)

Again on the CA2/Part1 folder

```
./gradlew runClient
```

![docker12](../report_images/docker12.png)

![docker13](../report_images/docker13.png)

Finding the container ID

```
docker container list --all
```

![docker14](../report_images/docker14.png)

```
docker kill c511f657a1b7
```

![docker15](../report_images/docker15.png)


### Pushing the images to Docker Hub

First register on the [Docker Hub](https://hub.docker.com/) website.

Login to docker hub

```
docker login
```

![docker16](../report_images/docker16.png)

Go again to the [Docker Hub](https://hub.docker.com/) website and create a repository

For this assignment the repository created was:
[nunofrribeiro/devops-21-22-lmn-1050317](https://hub.docker.com/repository/docker/nunofrribeiro/devops-21-22-lmn-1050317)

Know, to push the images to push the images to the repository use the tags:

```
docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
```

For the images created the tags where:

- Solution 1 

```
docker tag solution1:v0.1 nunofrribeiro/devops-21-22-lmn-1050317:v0.1
```

- Solution 2

```
docker tag solution2:v0.1 nunofrribeiro/devops-21-22-lmn-1050317:part1solution2
```

And know push the images to the repository

- Solution 1 

```
docker push nunofrribeiro/devops-21-22-lmn-1050317:v0.1
```

- Solution 1 

```
docker push nunofrribeiro/devops-21-22-lmn-1050317:part1solution2
```

Going to the repository:

![docker17](../report_images/docker17.png)


[^1]: Adapted from the devops07a file of the class Lecture 07a

[^2]: [From the Wikipedia Page of Docker](https://en.wikipedia.org/wiki/Docker_(software))

[^3]: [Docker reference builder site](https://docs.docker.com/engine/reference/builder/)
