CA5 - Technical Report
======================

## CA5 - Part 1 - CI/CD Pipelines with Jenkins

### 1. Analysis, design and implementation

For Part 1 of this assignment, the goal is to create a very simple pipeline (similar to the example from the
lectures).

So the assignment is divided in a few goals:

- Checkout. To checkout the code form the repository
Assemble. Compiles and Produces the archive files with the application. Do not use the build task of gradle (because it also executes the tests)!
- Test. Executes the Unit Tests and publish in Jenkins the Test results. See the junit step for further information on how to archive/publish test results.
    - Do not forget to add some unit tests to the project (maybe you already have done it).
- Archive. Archives in Jenkins the archive files (generated during Assemble)
- At the end of the part 1 of this assignment mark your repository with the tag
ca5-part1.

### 1.1. A brief explanation of Jenkins

Jenkins is an open source automation server. It helps automate the parts of software development related to building, testing, and deploying, facilitating continuous integration and continuous delivery. It is a server-based system that runs in servlet containers such as Apache Tomcat. It supports version control tools, including AccuRev, CVS, Subversion, Git, Mercurial, Perforce, ClearCase and RTC, and can execute Apache Ant, Apache Maven and sbt based projects as well as arbitrary shell scripts and Windows batch commands.[^1]

### 1.2 Using Jenkins

Jenkins can be intalled in many ways:

- Docker
- Kubernetes
- Linux
- macOS
- WAR files
- Windows

For this assignment we will be using Jenkins on a Docker container.

### 1.3 Installing Jekins In a Docker Container

By following the Jenkins documentation on the [Jenkins Website](https://www.jenkins.io/doc/book/installing/docker/)

1. First we need to create a bridge network in Docker

```
docker network create jenkins
```

By running this command

```
docker network ls
```

we can see the bridge network has been successfully created

![jenkins1](../report_images/jenkins1.png)

2. Run a docker:dind Docker image

```
docker run --name jenkins-docker --rm --detach --privileged --network jenkins --network-alias docker --env DOCKER_TLS_CERTDIR=/certs --volume jenkins-docker-certs:/certs/client --volume jenkins-data:/var/jenkins_home --publish 2376:2376 docker:dind
```

This will enable Jenkins to communicate with the "outside world".

3. Customise official Jenkins Docker image, by executing below two steps:

- Create Dockerfile with the following content:

```
    FROM jenkins/jenkins:2.332.3-jdk11

    USER root
    
    RUN apt-get update && apt-get install -y lsb-release
    RUN curl -fsSLo /usr/share/keyrings/docker-archive-keyring.asc \
    https://download.docker.com/linux/debian/gpg

    RUN echo "deb [arch=$(dpkg --print-architecture) \
    signed-by=/usr/share/keyrings/docker-archive-keyring.asc] \
    https://download.docker.com/linux/debian \
    $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list

    RUN apt-get update && apt-get install -y docker-ce-cli

    USER jenkins

    RUN jenkins-plugin-cli --plugins "blueocean:1.25.3 docker-workflow:1.28"
```

- Build a new docker image from this Dockerfile and assign the image a meaningful name, e.g. "myjenkins-blueocean:2.332.3-1":

```
docker build -t myjenkins-blueocean:2.332.3-1 .
```

To see if the image was build

```
docker image ls
```

![jenkins2](../report_images/jenkins2.png)

4. Run your own myjenkins-blueocean:2.332.3-1 image as a container in Docker using the following docker run command:

```
docker run --name jenkins-blueocean --restart=on-failure --detach --network jenkins --env DOCKER_HOST=tcp://docker:2376 --env DOCKER_CERT_PATH=/certs/client --env DOCKER_TLS_VERIFY=1 --volume jenkins-data:/var/jenkins_home --volume jenkins-docker-certs:/certs/client:ro --publish 8082:8080 myjenkins-blueocean:2.332.3-1
```

5. Accessing the docker container

```
docker exec -it jenkins-blueocean bash
```

6. Accessing the Docker logs

First run:

```
docker container ls
```

to list all running containers, and get the jenkins container name.

Then just run the command:

```
docker logs <docker-container-name>
```

### 1.4 Post-installation setup wizard [^2]

- Go to http://localhost:8082 and wait until the **Unlock Jenkins** page appears.

![Jenkins3](../report_images/jenkins3.png)

- From the Jenkins console log output, copy the automatically-generated alphanumeric password (between the 2 sets of asterisks).

![jenkins4](../report_images/jenkins4.png)

- On the Unlock Jenkins page, paste this password into the Administrator password field and click Continue.

### 1.5. Jenkins plugins to be used

To run the pipelines for the two class assignments we will be creating we need to add the following plugins:

- Bitbucket Plugin
- Docker Pipeline
- Docker plugin
- HTML Publisher plugin
- Javadoc plugin
- Junit

### 2. Analysis, design and implementation

#### 2.1. Creating a pipeline

On the Jenkins dashboard go to "New Item" and create a new Pipeline

![jenkins5](../report_images/jenkins5.png)

![jenkins6](../report_images/jenkins6.png)

On the pipeline section will be selecting the following options:

- Pipeline from SCM
- SCM: in thi case Git
- Link to my personal repository
- And add the bitbucket credentials on "Manage Jekins", then "Manage Credentials" and finally adding the credetials to Jenkins

![jenkins7](../report_images/jenkins7.png)

We also need to Jenkins the location of the Jenkinsfile.

![jenkins9](../report_images/jenkins9.png)

#### 2.2. Pipeline Script (Jenkinsfile)

The script written to the **Jenkinsfile** needs to be added to the root folder of CA2/Part1/gradle_basic_demo of the repository [3f8cc53](https://bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317/commits/3f8cc53510efed002a8ebd93dc9b355d076eb4c2)

- Checkout section

```
stage('Checkout') {
            steps {
                echo 'Checking out....'
                git credentialsId: 'bitbucket-credentials', url: 'https://N_Ribeiro@bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317.git'
                dir("ca2/Part1/gradle_basic_demo"){
                    sh 'pwd'
                }
            }
        }
```

This step will clone the repository into the Jekins container and use the credentials stored in the Credentials section

- Assemble

```
stage('Assemble') {
            steps {
                echo 'Assembling....'
                dir("ca2/Part1/gradle_basic_demo"){
                    sh 'pwd'
                    script {
                        sh 'chmod u+x ./gradlew'
                        sh './gradlew assemble'
                    }
                }
            }
        }
```

The "dir" section will tell Jenkins the directory it will run the commands on the script.

First we need to give execution permission to the Gradle wrapper

Then we can run the Assemble task from gradle (this will build the project, but won't run the tests).

- Test

```
stage('Test') {
            steps {
                echo'Performing tests....'
                dir("ca2/Part1/gradle_basic_demo"){
                    sh 'pwd'
                    script {
                        sh './gradlew check'
                        sh './gradlew testClasses'
                    }
                }
            }
        }
```

I every step we need to tell Jenkins the directory we run the commands 

Then we can run the testClasses task.

- Archiving

```
stage('Archiving'){
            steps {
                echo 'Archiving....'
                dir("ca2/Part1/gradle_basic_demo"){
                    sh 'pwd'
                    script {
                        archiveArtifacts 'build/distributions/'
                    }
                }
            }
        }
```

This step will Archive the Jars generated, and stored in the folder "build/distributions/"

- Publish the tests

Lastly we will add a Post Section to fetch the .xml reports generated by the testClasses task and publish to Jenkins

```
post {
        always {
            dir("ca2/Part1/gradle_basic_demo"){
                    script {
                        junit testResults: '**/test-results/**/*.xml'
                    }
                }
        }
    }
```

- Entire pipeline script

The final pipeline script will look like this:

```
pipeline {
    agent any
    
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out....'
                git credentialsId: 'bitbucket-credentials', url: 'https://N_Ribeiro@bitbucket.org/n_ribeiro/devops-21-22-lmn-1050317.git'
                dir("ca2/Part1/gradle_basic_demo"){
                    sh 'pwd'
                }
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling....'
                dir("ca2/Part1/gradle_basic_demo"){
                    sh 'pwd'
                    script {
                        sh 'chmod u+x ./gradlew'
                        sh './gradlew assemble'
                    }
                }
            }
        }
        stage('Test') {
            steps {
                echo'Performing tests....'
                dir("ca2/Part1/gradle_basic_demo"){
                    sh 'pwd'
                    script {
                        sh './gradlew check'
                        sh './gradlew testClasses'
                    }
                }
            }
        }
        stage('Archiving'){
            steps {
                echo 'Archiving....'
                dir("ca2/Part1/gradle_basic_demo"){
                    sh 'pwd'
                    script {
                        archiveArtifacts 'build/distributions/'
                    }
                }
            }
        }
    }

    post {
        always {
            dir("ca2/Part1/gradle_basic_demo"){
                    script {
                        junit testResults: '**/test-results/**/*.xml'
                    }
                }
        }
    }
}
```

#### 2.3. Running the Build

On the Dashboard we can run the script by going to "Build Now"

After the Build has finished we can check the results on the Pipeline Page we created.

![jenkins8](../report_images/jenkins8.png)

[^1]: [From the wikipedia page of Jenkins](https://en.wikipedia.org/wiki/Jenkins_(software))

[^2]: [Unlocking Jenkins](https://www.jenkins.io/doc/book/installing/docker/#unlocking-jenkins)
