package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void compareEmployees() {
        //Arrange
        String firstName = "Nuno";
        String lastName = "Ribeiro";
        String description = "Student";
        //Act
        Employee newEmployee = new Employee(firstName, lastName, description);
        Employee testEmployee = new Employee(firstName, lastName, description);
        //Assert
        assertEquals(newEmployee.toString(), testEmployee.toString());
    }
}
